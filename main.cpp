#include <iostream>
using namespace std;

#include "colors.h"


int main(int argc, char* argv[])
{
    cerr << "═════════════════════════════════════════════════════════════════"
         << endl;
    cerr << "Number of arguments: " << CYAN << argc << RESETCOLOR << endl
         << endl;

    for (int i=0; i<argc; i++)
    {
        cerr << "Argument #" << i << ": " 
             << GREEN << argv[i] << RESETCOLOR << endl;
    }

    cerr << "═════════════════════════════════════════════════════════════════"
         << endl
         << endl;

    /* Your code goes here! */

    return 0;
}



This repo contains some skeleton C++ files for a new project, including the use of ANSI terminal colors.
Quite useful and convenient if you find it tedious to manually set up those files, and want to make colorful console output!

## Setting Up

If you have a new project named, for example, `hacking`, you can create a skeleton for it using the following command:

```shell
$ git clone https://git.mst.edu/djmvfb/cpp_skeleton.git hacking
```

## Compilation

After that, just `cd` into the `hacking/` directory, and `make` to compile your code.

```shell
$ cd hacking
hacking$ make
/usr/bin/g++ -c -g -O0 -Wall -W -pedantic-errors main.cpp -o main.o
/usr/bin/g++ -g -O0 -Wall -W -pedantic-errors main.o -o driver
```

## Execution

To execute your new project, just call `./driver`.

```shell
$ ./driver 
═════════════════════════════════════════════════════════════════
Number of arguments: 1

Argument #0: ./driver
═════════════════════════════════════════════════════════════════

```


## Cleaning Up

To clean up the intermediate compilation files, just type `make clean`.

```shell
hacking$ ls
colors.h  driver  LICENSE  main.cpp  main.o  Makefile  README.md
hacking$ make clean
hacking$ ls
colors.h  LICENSE  main.cpp  Makefile  README.md
```

Happy hacking!

